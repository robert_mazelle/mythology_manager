package com.hibernateapp;


import javax.persistence.*;

@Entity
@Table(name = "gods")
public class God {

    @Id
    private int id;
    private String name;
    private String attribute;
    @ManyToOne
    private Mythology mythology;

    public God(int id, String name, String attribute, Mythology mythology) {
        this.id = id;
        this.name = name;
        this.attribute = attribute;
        this.mythology = mythology;
    }

    public God() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public Mythology getMythology() {
        return mythology;
    }

    public void setMythology(Mythology mythology) {
        this.mythology = mythology;
    }

    @Override
    public String toString() {
        return "God: " + name + "(ID: " + id + ") from the " + mythology.getName() + " mythology. This God's attribute is " + attribute;
    }
}

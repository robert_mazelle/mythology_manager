package com.hibernateapp;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MYTHOLOGIES")
public class Mythology {

    @Id
    private int id;
    private int startCentury;
    private int endCentury;
    private String name;
    @OneToMany(mappedBy = "mythology")
    private List<God> gods;

    public Mythology(int id, String name, int startCentury, int endCentury) {
        this.startCentury = startCentury;
        this.endCentury = endCentury;
        this.id = id;
        this.name = name;
    }

    public Mythology() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStartCentury() {
        return startCentury;
    }

    public void setStartCentury(int startCentury) {
        this.startCentury = startCentury;
    }

    public int getEndCentury() {
        return endCentury;
    }

    public void setEndCentury(int endCentury) {
        this.endCentury = endCentury;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<God> getGods() {
        return gods;
    }

    public void setGods(List<God> gods) {
        this.gods = gods;
    }

    @Override
    public String toString() {
        if(endCentury == 99 && startCentury == 99) return name + " Mythology, unknown time interval";
        return name + " Mythology (Start Century: " + startCentury + ", End Century: " + endCentury + ')';
    }
}

package com.hibernateapp;

import jdk.internal.util.xml.impl.Input;

import javax.persistence.*;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class App {

    static EntityManager entityManager;

    public static void main(String[] main) {

        Scanner scanner = new Scanner(System.in);
        Scanner scannerFinish = new Scanner(System.in);

        String closeApp = "";

        int readInt = 0;

        App app = new App();

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("pu");
        entityManager = entityManagerFactory.createEntityManager();

        List<God> gods = app.updateList("God");
        List<Mythology> mythologies = app.updateList("Mythology");



        do {

            System.out.println("What do you want to do?\n" +
                    "1. Insert/modify God\n" +
                    "2. Insert/modify Mythology\n" +
                    "3. Remove God\n" +
                    "4. Remove Mythology\n" +
                    "5. Print Gods/Mythologies");

            readInt = scanner.nextInt();

            switch (readInt) {
                case 1:
                    gods = app.updateGodList(gods, mythologies);
                    break;
                case 2:
                    mythologies = app.updateMythologyList(mythologies);
                    break;
                case 3:
                    gods = app.removeGod(gods);
                    break;
                case 4:
                    mythologies = app.removeMythology(mythologies);
                    break;
                case 5:
                    app.printManager(gods, mythologies);
                    break;
                default:
                    System.out.println("Do you want to close the app? Y/y");
                    closeApp = scannerFinish.nextLine();
            }
        } while (!closeApp.equals("y") && !closeApp.equals("Y"));

    }

    public List<God> updateGodList(List<God> gods, List<Mythology> mythologies) {

        Scanner scanner = new Scanner(System.in);
        Scanner intScanner = new Scanner(System.in);
        int input = 0;
        String readLine;
        boolean convertingFailure = false;
        God foundGod;

        System.out.println("You are in the god manager.");

        do {
            System.out.println("You can:\n1. Insert God\n2. Update God\nIf you want to finish - insert any letter");
            readLine = scanner.nextLine();

            if (readLine.equals("2")) {
                System.out.println("Which God do you want to update? Insert ID or name");
                readLine = intScanner.nextLine();
                foundGod = findGod(gods, readLine);
                if (foundGod != null) insertGod(gods, mythologies, foundGod);
                else System.out.println("There is no such God in the database.");
            }else if (readLine.equals("1"))
                    gods = insertGod(gods, mythologies, null);
            } while (readLine.equals("1") || readLine.equals("2")) ;
        return gods;
    }

    public List<Mythology> updateMythologyList(List<Mythology> mythologies) {

        Scanner scanner = new Scanner(System.in);
        Scanner intScanner = new Scanner(System.in);
        String readLine;
        Mythology inputMythology;
        boolean convertingFailure = false;

        System.out.println("You are in the Mythology manager.");

        do {
            System.out.println("You can:  \n1. Insert Mythology\n2. Update Mythology \nIf you want to finish - insert any letter");
            readLine = scanner.nextLine();
            if (readLine.equals("2")) {
                System.out.println("Which Mythology do you want to update? Insert ID or name");
                readLine = intScanner.nextLine();
                inputMythology = findMythology(mythologies, readLine);
                if (inputMythology != null) mythologies = insertMythology(mythologies, inputMythology);
                else System.out.println("There is no such mythology in the database, you cannot update it!");
            } else if (readLine.equals("1"))
                mythologies = insertMythology(mythologies, null);
        } while (readLine.equals("1") || readLine.equals("2"));
        return mythologies;
    }


    public List updateList(String whichList) {

        if (whichList.equals("Mythology")) {
            TypedQuery<Mythology> queryM = entityManager.createQuery("select m from Mythology m", Mythology.class);
            return queryM.getResultList();
        } else if (whichList.equals("God")) {
            TypedQuery<God> queryG = entityManager.createQuery("select g from God g", God.class);
            return queryG.getResultList();
        }

        return null;
    }

    public List<God> removeGod(List<God> gods) {
        Scanner scanner = new Scanner(System.in);
        God foundGod;
        String readLine;
        System.out.println("Which God do you want to remove? Insert ID or name");
        readLine = scanner.nextLine();

        foundGod = findGod(gods, readLine);

        if (foundGod == null) {
            System.out.println("There is no such God.");
        }

        else {
            God removeGod = entityManager.find(God.class, foundGod.getId());
            entityManager.getTransaction().begin();
            try {
                entityManager.remove(removeGod);
                entityManager.getTransaction().commit();
                gods.remove(foundGod);
                System.out.println("God successfully removed from the database");
            } catch (RollbackException re) {
                System.out.println("Something went wrong, try again");
            }

        }
        return gods;
    }

    public List<Mythology> removeMythology(List<Mythology> mythologies) {
        Scanner scanner = new Scanner(System.in);
        Mythology foundMythology;
        String readLine;
        System.out.println("Which Mythology do you want to remove? Insert ID o name.");

        readLine = scanner.nextLine();
        foundMythology = findMythology(mythologies, readLine);

        if (foundMythology == null) System.out.println("There is no such Mythology.");
        else {
            Mythology removeMythology = entityManager.find(Mythology.class, foundMythology.getId());
            entityManager.getTransaction().begin();
            try {
                entityManager.remove(removeMythology);
                entityManager.getTransaction().commit();
                mythologies.remove(foundMythology);
                System.out.println("Mythology successfully removed from the database");
            } catch (RollbackException re) {
                System.out.println("You need to remove the Gods connected to this Mythology first.");
            }

        }
        return mythologies;
    }

    public List<God> insertGod(List<God> gods, List<Mythology> mythologies, God god) {
        Scanner scanner = new Scanner(System.in);
        int indexInsert;
        boolean continueIntegerInput = true;
        String readLine;

        if (god == null) indexInsert = findFreeIndex("God");
        else indexInsert = god.getId();


                System.out.println("Insert the God's data as follows: Name, Attribute");
                readLine = scanner.nextLine();
                String[] data = readLine.split(", ");

                if (data.length == 2) {

                    System.out.println("To which mythology belongs this God? Insert ID or name");
                    readLine = scanner.nextLine();
                    Mythology newMythology = findMythology(mythologies, readLine);
                    if (newMythology == null) {
                        continueIntegerInput = false;
                        System.out.println("There is no such mythology");
                    }

                    if (continueIntegerInput) {

                            God newGod = new God(indexInsert, data[0], data[1], newMythology);
                            entityManager.getTransaction().begin();
                            try {  //the NAME column is unique, so you cannot insert two the same gods
                                entityManager.merge(newGod);
                                entityManager.getTransaction().commit();
                                gods.add(newGod);
                                if (god != null) {
                                    gods.remove(god);
                                    System.out.println("God successfully updated.");
                                }
                                else System.out.println("God successfully added.");
                            } catch (PersistenceException pe) {
                                System.out.println("This god already exists! Try again.");
                            }

                        }
                    } else System.out.println("Provided invalid data format");
        return gods;
    }

    public List<Mythology> insertMythology(List<Mythology> mythologies, Mythology mythology) {

        Scanner scanner = new Scanner(System.in);
        int index = 0;

        if (mythologies == null) index = findFreeIndex("Mythology");
        else index = mythology.getId();


            System.out.println("Insert the Mythology's data as follows: Name, Start Century, End Century (If you don't know the timeline just type 99 in the centuries)");
            String readLine = scanner.nextLine();
            String[] data = readLine.split(", ");

            if (data.length == 3) {
                int startCentury = 0;
                int endCentury = 0;

                try {
                    startCentury = Integer.parseInt(data[1]);
                    endCentury = Integer.parseInt(data[2]);
                } catch (NumberFormatException nfe) {
                    System.out.println("Invalid data format, try again");
                }

                Mythology newMythology = new Mythology(index, data[0], startCentury, endCentury);

                try {  //the NAME column is unique, so you cannot insert two the same mythologies
                    mythologies.add(newMythology);
                    entityManager.getTransaction().begin();
                    entityManager.merge(newMythology);
                    entityManager.getTransaction().commit();
                    if (mythology != null) {
                        mythologies.remove(mythology);
                        System.out.println("Mythology successfully updated.");
                    }
                    else System.out.println("Mythology succesfully added.");
                } catch (PersistenceException pe) {
                    System.out.println("This mythology already exists! Try again.");
                }

            } else System.out.println("Provided invalid data format ");

        return mythologies;
    }

    public int findFreeIndex(String whichEntity) {
        int freeIndex = 1;
        if (whichEntity.equals("Mythology")) {
            while (entityManager.find(Mythology.class, freeIndex) != null) {
                freeIndex++;
            }
        }
        else if (whichEntity.equals("God")) {
            while (entityManager.find(God.class, freeIndex) != null) {
                freeIndex++;
            }
        }
        return freeIndex;
    }

    public void printGods(List<God> gods, List<Mythology> mythologies, int whichMythology) {
        Mythology chosenMythology = null;

        if (whichMythology > 0) {

            for (Mythology m : mythologies) {
                if (m.getId() == whichMythology) chosenMythology = m;
            }

            if (chosenMythology != null) {
                for (God g : gods) {
                    if (g.getMythology() == chosenMythology) System.out.println(g);
                }
            }

            else System.out.println("There is no mythology with this ID.");
        }
        else {
            for (God g : gods) System.out.println(g);
        }
    }

    public void printMythologies(List<Mythology> mythologies) {
        for (Mythology m : mythologies) System.out.println(m);
    }

    public void printGods(List<God> gods, List<Mythology> mythologies, String whichMythology) {
        Mythology chosenMythology = null;
        for (Mythology m : mythologies) if (m.getName().equals(whichMythology)) chosenMythology = m;

        if (chosenMythology != null) {
            for (God g : gods) {
                if (g.getMythology() == chosenMythology) System.out.println(g);
            }
        }
        else System.out.println("There is no mythology of this name");
    }

    public void printManager(List<God> gods, List<Mythology> mythologies) {
        Scanner intScanner = new Scanner(System.in);
        Scanner scanner = new Scanner(System.in);
        String inputString = null;
        boolean convertSuccesful = true;
        int input = 0;

        System.out.println("What do you want to print?\n1. Mythologies\n2. Gods");

        try {
            input = intScanner.nextInt();
        } catch (InputMismatchException ime) {
            System.out.println("You need to insert a number!");
        }
        if (input == 1) {
            printMythologies(mythologies);
        }
        if (input == 2) {
            System.out.println("Insert the name of the Mythology or it's ID to print all of the God's of this Mythology, or type \"all\" if you want to print all of them apart of the Mythology");
            inputString = scanner.nextLine();
            try {
                input = Integer.parseInt(inputString);
                printGods(gods, mythologies, input);
            } catch(NumberFormatException nfe) {
                convertSuccesful = false;
            }
            if (!convertSuccesful) {
                if (inputString.equals("all")) printGods(gods, mythologies, -1);
                else printGods(gods, mythologies, inputString);
            }
        }
    }

    public Mythology findMythology(List<Mythology> mythologies, String nameOrID) {
    Mythology chosenMythology = null;
    int mythologyIndex = 0;
    boolean convertingFailure = false;
        try {
            mythologyIndex = Integer.parseInt(nameOrID);
        } catch (NumberFormatException exception) {
            convertingFailure = true;
        }

        if (convertingFailure) {
            for (Mythology m : mythologies)
                if (m.getName().equals(nameOrID)) {
                    chosenMythology = m;
                    break;
                }
        }
        else {
            for (Mythology m : mythologies)
                if (m.getId() == mythologyIndex) {
                    chosenMythology = m;
                    break;
                }
        }
        return chosenMythology;
    }

    public God findGod(List<God> gods, String nameOrID) {
        int godIndex = 0;
        boolean convertingFailure = false;
        God chosenGod = null;
        try {
            godIndex = Integer.parseInt(nameOrID);
        } catch (NumberFormatException exception) {
            convertingFailure = true;
        }

        if (convertingFailure) {
            for (God g : gods)
                if (g.getName().equals(nameOrID)) {
                    chosenGod = g;
                    break;
                }
        }
        else {
                    for (God g : gods)
                        if (g.getId() == godIndex) {
                            chosenGod = g;
                            break;
                        }
                }

        return chosenGod;
    }


}
